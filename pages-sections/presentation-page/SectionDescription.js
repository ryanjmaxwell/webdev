import React from "react";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import InfoArea from "components/InfoArea/InfoArea.js";
import PageviewOutlinedIcon from '@material-ui/icons/PageviewOutlined';

// @material-ui icons
import CloudIcon from '@material-ui/icons/Cloud';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import DeveloperModeIcon from '@material-ui/icons/DeveloperMode';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import descriptionStyle from "assets/jss/nextjs-material-kit-pro/pages/presentationSections/descriptionStyle.js";

const useStyles = makeStyles(descriptionStyle);

export default function SectionDescription() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <div className={classes.container}>
        <GridContainer justify="center">
          <GridItem md={8} sm={8}>
            <h4 className={classes.description}>
              I pride myself on hardwork and meeting deadlines.             
            </h4>
          </GridItem>
        </GridContainer>
        <div className={classes.features}>
          <GridContainer>
            <GridItem md={4} sm={4}>
              <InfoArea
                title="Latest Skills"
                description="Working with the latest frameworks and ES6+ javascript language updates, I have developed web sites with SEO and future updates/upgrades in mind. I have developed using the basic React framework, but also NextJS. When developing using sensors I use Python and Django."
                icon={DeveloperModeIcon}
                iconColor="primary"
                vertical={true}
              />
            </GridItem>
            <GridItem md={4} sm={4}>
              <InfoArea
                title="Leadership"
                description="With years of experience leading Production Assurance meetings with Canada Revenue Agency and leading over 10 workers on construction sites, I have built many skills to get the job done and provide a communicative, fun but safe work environment."
                icon={ShowChartIcon}
                iconColor="primary"
                vertical={true}
              />
            </GridItem>
            <GridItem md={4} sm={4}>
              <InfoArea
                title="Cloud"
                description="Working mostly with the AWS cloud platform, I develop my applicaitons to utilize as many services as possible. Working in the console but with the CLI and API's have allowed me to get a indepth understanding of how many of the AWS services function. A few of the services I use are : S3, Route53, Amplify, IAM, CloudWatch and DynamoDB"
                icon={CloudIcon}
                iconColor="primary"
                vertical={true}
              />
            </GridItem>
          </GridContainer>
        </div>
      </div>
    </div>
  );
}
