/*eslint-disable*/
import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components

import Parallax from "components/Parallax/Parallax.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";

//import Modal from "components/Modal/AgeGate.js"

// sections for this page
import SectionDescription from "pages-sections/presentation-page/SectionDescription.js";
import presentationStyle from "assets/jss/nextjs-material-kit-pro/pages/presentationStyle.js";

import FaceRyan from 'assets/img/4.jpg'

const useStyles = makeStyles(presentationStyle);

export default function PresentationPage() {
  React.useEffect(() => {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  });
  const classes = useStyles();
  return (
    <div>
      
      
      <Parallax
        image={require("assets/img/ITbkground_min.jpeg")}
        className={classes.parallax}
      >
       
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
      <GridContainer>
         
      <GridItem className={classNames(
            classes.mrAuto,
            classes.mlAuto,
            classes.textCenter
            )}>
          <Card profile plain>
            <CardAvatar profile plain>
              <a href="#pablo">
                <img src={FaceRyan} className={classes.img} />
              </a>
            </CardAvatar>
            <CardBody plain>
            <SectionDescription />
            </CardBody>
            
          </Card>
        </GridItem>
        </GridContainer>
        
       
        
       
      </div>
      
      
    </div>
  );
}
