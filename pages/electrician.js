/*eslint-disable*/ import React, {useState} from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import Mail from "@material-ui/icons/Mail";
// core components

import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import MyAccordion from "components/Accordion/MyAccordion.js";
import Parallax from "components/Parallax/Parallax.js";

import data from '../assets/json/faq.json';


import landingPageStyle from "assets/jss/nextjs-material-kit-pro/pages/faqStyle.js";


const useStyles = makeStyles(landingPageStyle);

export default function FAQPage({ ...rest }) {
  
  React.useEffect(()=> {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    
  }, []);

  const classes = useStyles();
  return (
    <div>
    
      <Parallax image={require("assets/img/electricity.jpg")} filter="dark">
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={6} md={6}>
              <h1 className={classes.title}>Licensed Electrician</h1>
          
              
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
      
        <h2 className={classNames(classes.headingTitle, classes.mlAuto, classes.mrAuto, classes.textCenter)}>My Work History</h2>
    
        <div className={classes.container}>
            
          
          <div id="collapse" className={classes.collapse}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                
              <MyAccordion
                active={0}
                activeColor="primary"
                collapses = {data}
              />
                 
                
                
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
      
    </div>
  );

}
