/*eslint-disable*/
import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components used to create a google map
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import PinDrop from "@material-ui/icons/PinDrop";
import Phone from "@material-ui/icons/Phone";
import CloudIcon from '@material-ui/icons/Cloud';
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import InfoArea from "components/InfoArea/InfoArea.js";

import contactUsStyle from "assets/jss/nextjs-material-kit-pro/pages/contactUsStyle.js";

const CustomSkinMap = withScriptjs(
  withGoogleMap(() => (
    <GoogleMap
      defaultZoom={16}
      defaultCenter={{ lat: 45.268230, lng: -75.732790 }}
      defaultOptions={{
        scrollwheel: false,
        zoomControl: true,
        styles: [
          {
            featureType: "water",
            stylers: [
              { saturation: 43 },
              { lightness: -11 },
              { hue: "#0088ff" }
            ]
          },
          {
            featureType: "road",
            elementType: "geometry.fill",
            stylers: [
              { hue: "#ff0000" },
              { saturation: -100 },
              { lightness: 99 }
            ]
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#808080" }, { lightness: 54 }]
          },
          {
            featureType: "landscape.man_made",
            elementType: "geometry.fill",
            stylers: [{ color: "#ece2d9" }]
          },
          {
            featureType: "poi.park",
            elementType: "geometry.fill",
            stylers: [{ color: "#ccdca1" }]
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#767676" }]
          },
          {
            featureType: "road",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#ffffff" }]
          },
          { featureType: "poi", stylers: [{ visibility: "ON" }] },
          {
            featureType: "landscape.natural",
            elementType: "geometry.fill",
            stylers: [{ visibility: "on" }, { color: "#b8cb93" }]
          },
          { featureType: "poi.park", stylers: [{ visibility: "on" }] },
          {
            featureType: "poi.sports_complex",
            stylers: [{ visibility: "on" }]
          },
          { featureType: "poi.medical", stylers: [{ visibility: "on" }] },
          {
            featureType: "poi.business",
            stylers: [{ visibility: "simplified" }]
          }
        ]
      }}
    >
      <Marker position={{ lat: 45.268230, lng: -75.732790}} />
    </GoogleMap>
  ))
);

const useStyles = makeStyles(contactUsStyle);

import FaceRyan from 'assets/img/4.jpg'


export default function ContactUsPage() {
  React.useEffect(() => {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  });
  const classes = useStyles();
  return (
    <div>
     
      <div className={classes.bigMap}>
        <CustomSkinMap
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCn5EaeY_iyPbwXe7ipXVhArPWrXDcBLr8"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={
            <div
              style={{
                height: `100%`,
                borderRadius: "6px",
                overflow: "hidden"
              }}
            />
          }
          mapElement={<div style={{ height: `100%` }} />}
        />
      </div>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.contactContent}>
          <div className={classes.container}>
            
          <GridContainer>
         
         <GridItem className={classNames(
               classes.mrAuto,
               classes.mlAuto,
               classes.textCenter
               )}>
             <Card profile plain>
               <CardAvatar profile raised>
                 <a href="#pablo">
                   <img src={FaceRyan} className={classes.img} />
                 </a>
               </CardAvatar>
               <CardBody plain>
               <InfoArea
                  className={classes.info}
                  title="Mail me:"
                  description={
                    <p>
                      367 Balinroan Cres, <br /> Nepean, ON K2J 3V1,{" "}
                      <br /> Canada
                    </p>
                  }
                  icon={PinDrop}
                  iconColor="primary"
                />
                <InfoArea
                  className={classes.info}
                  title="Give me a call or text:"
                  description={
                    <div>
                      <p>
                        Mobile <br /> +1-613-979-6764 <br />
                    </p>
                    </div>
                  }
                  icon={Phone}
                  iconColor="primary"
                />
                <InfoArea
                  className={classes.info}
                  title="Email me:"
                  description={
                    <div>
                      <p>
                        Email <br /> ryan@ryanjmaxwell.ca<br />
                    </p>
                    </div>
                  }
                  icon={CloudIcon}
                  iconColor="primary"
                />
                <InfoArea
                  className={classes.info}
                  title="LinkedIn:"
                  description={
                    <div>
                      <p>
                        LinkedIn <br /> <a href="https://www.linkedin.com/in/ryan-maxwell-9a25325a/" >Ryan on LinkedIn</a><br />
                    </p>
                    </div>
                  }
                  icon={CloudIcon}
                  iconColor="primary"
                />
               </CardBody>
               
             </Card>
           </GridItem>
           </GridContainer>
                
          
          </div>
        </div>
      </div>
      
    </div>
  );
}
