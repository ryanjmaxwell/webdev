import React, { Component } from "react";
//Next components
import Router from "next/router";
import cookies from 'next-cookies';
//material-ui/core items
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from '@material-ui/core/DialogActions';
import Checkbox from '@material-ui/core/Checkbox';
import DialogContentText from '@material-ui/core/DialogContentText';
//custom component
import Button from "components/CustomButtons/Button.js";
// custome styles
import modalStyle from "assets/jss/nextjs-material-kit-pro/modalStyle.js";
import { Typography } from "@material-ui/core";

const theme = {
  background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
};

const styles = () => ({
  ...modalStyle(theme)
});

class Index extends Component {
  static async getInitialProps(ctx) {

  if(cookies(ctx).visited ){
    return {
      cookie: true
      }
  }
    return {
      cookie:false      
  }    

}

  constructor(props){
    super(props);
       
    this.state = {
        show:true,
        checked: false,
        key : 'visited',
        cookie : props.cookie
    }

  this.handleSubmit = this.handleSubmit.bind(this);
  this.handleNoSubmit = this.handleNoSubmit.bind(this);
  this.rememberMe = this.rememberMe.bind(this);
}

classes = styles();

handleClose = () => null;

componentDidMount(){ 

  const locStorage = document.cookie.split(';').some((item) => item.trim().startsWith('visited='));
     
  if (locStorage) {
    
    Router.push("/home");
    
  }
  
  

} 

setAgeGate = ()=> {
  let now = new Date();
  // `item` is an object which contains the original value
  // as well as the time when it's supposed to expire
  const item = {
    value: true,
    expiry: new Date(now.getTime() + 25920000).toUTCString(), //1000 * 60 * 60 * 24 * 3 = 3 days in miliseconds
    
  }
  const cookieString = this.state.key + '=true; path=/; expires=' + item.expiry;
  document.cookie = cookieString;
  
  
      return true;
}

handleSubmit(){
  
  if (this.state.checked){
    this.setState({show:false});  
    this.setAgeGate();
              
  }
  Router.push("/programmer");
}

handleNoSubmit(){
  if (this.state.checked){
    this.setState({show:false});  
    this.setAgeGate();
              
  }
  Router.push("/electrician");


}

rememberMe(){
  this.setState({checked:!this.state.checked});

}


render(){
return(
<div>
  {this.state.show?
<Dialog maxWidth="sm" scroll="paper" onClose={this.handleClose} aria-labelledby="simple-dialog-title" open={this.state.show}>
  <DialogTitle className={this.classes.modalHeader} id="simple-dialog-title" align="center">Welcome to Ryan Maxwell's website!! </DialogTitle>
            <DialogContentText className={this.classes.modalBody} align="center">
          <Typography>Please pick your primary reason for visiting:</Typography>
          <Typography >(Both are accessible from within)</Typography>
        </DialogContentText>
  <DialogActions>
          <Button color="success"
          fullWidth
          block
          onClick={this.handleSubmit}>Programmer Analyst</Button>
  </DialogActions>
  <DialogActions>
        <Button color="info" 
          fullWidth
          block 
          onClick={this.handleNoSubmit}>Licenced Electrician</Button>
      </DialogActions>
      <DialogContentText align="center">
          <Checkbox
            checked={this.state.checked}
            onChange={this.rememberMe}
            name="rememberMe"
            color="default"
            inputProps={{ 'aria-label': 'Remember My choice for 3 days' }}
      /> Remember my choice for 3 days 
        </DialogContentText>
        <DialogContentText align="center">
          By entering the site you agree to the use of cookies.
        </DialogContentText>
        
      
    </Dialog>
    :null}
</div>
  
)
}
} export default Index;
