import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import { blue } from '@material-ui/core/colors';


//import styles from '../../styles/AgeGate.module.css'

class AgeGate extends Component{
    constructor(){
        super();

        
        this.state = {
            provinces : ["YT","BC", "NT", "AB","SK","NU","MB","ON","QC","NB","PE","NS","NL" ],
            show:undefined,
            yearSlider:undefined,
            provSlider:12,
            ageGate:19,
            monSlider:12,
            daySlider:31,
            showError:false,
            showMonth:false,
            showDay:false,
            ageConsent:undefined,
            currentYear : new Date().getFullYear(new Date()),
            currentMon : new Date().getMonth(new Date())+1,
            currentDay : new Date().getDate(new Date()),
            minYear : undefined,
            key : 'visited'
        }

        

        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    

    handleClose = () => null;

    getLocalStorage =()=>{
        const itemStr = localStorage.getItem(this.state.key)
        
        if(!itemStr){
            return false;
        }else{
            const item = JSON.parse(itemStr)
            const now = new Date()
            // compare the expiry time of the item with the current time
            if (now.getTime() > item.expiry) {
                // If the item is expired, delete the item from storage
                // and set state to proper values
                localStorage.removeItem(this.state.key)
                return false;   
            }else{
              return true;
            }
        }
        
    }

    componentDidMount(){
        this.setState({yearSlider:this.state.currentYear});
        this.setState({minYear:this.state.currentYear-90});
        this.setState({ageConsent:new Date().getFullYear(new Date())-19});
          
       const locStorage = this.getLocalStorage();
       
       if (locStorage){
        
        this.setState({show:false});

       }
       
        else{
            
            this.setState({show:true});

        }
        

    } 

    setAgeGate = ()=> { 
      const now = new Date()

      // `item` is an object which contains the original value
      // as well as the time when it's supposed to expire
      const item = {
        value: 'true',
        expiry: now.getTime() + 360000, //2592000000 -> 30 days
      }
          localStorage.setItem(this.state.key, JSON.stringify(item))
          return true;
    }
    
    handleSubmit = () => {
      
        
          
          if(this.state.yearSlider != this.state.ageConsent){
  
            if (this.state.yearSlider < this.state.ageConsent){
                
              const setAge = this.setAgeGate();

              if (setAge){
                 
                this.setState({show:false});
              }
              
            }else{
              
              this.setState({showError:true});
              
            }      
          }else{
            this.setState({showMonth:true});
          }
            
          if (this.state.showMonth){
            if(this.state.monSlider != this.state.currentMon){
              if(this.state.monSlider > this.state.currentMon){
                     
                this.setState({showError:true});
                }else{
                    
                    const setAge = this.setAgeGate();
      
                    if (setAge){
                        
                      this.setState({show:false});
                    }
                }
            }
              
              this.setState({showDay:true});
          }

          if (this.state.showDay){
            if(this.state.daySlider != this.state.currentDay){
                       
        
              if(this.state.daySlider > this.state.currentDay){
                this.setState({showError:true});
              }else{
                
              const setAge = this.setAgeGate();

              if (setAge){
                  
                
                this.setState({show:false});
              }
              }
            }else{
                
                this.setAgeGate();
                alert("Happy BirthDay!!");
                this.setState({show:false});
              }
          }
        }
        
    
      
    handleChangeSlider=(e)=>{
      this.setState({
        yearSlider:parseInt(e.target.value)});
       
    }
    
    handleProvChangeSlider=(e)=>{
      const intValue = parseInt(e.target.value);
      
      if (intValue != 8 || intValue != 3) {
        this.setState({ageGate:19});
        this.setState({ageConsent:this.state.currentYear - 19});
      }
        if (intValue === 8) {
            this.setState({ageGate:21});
            this.setState({ageConsent:this.state.currentYear - 21});
          
          
        }
        if (intValue === 3){
          
            this.setState({ageGate:18});
            this.setState({ageConsent:this.state.currentYear - 18});
        }
  
      
        
      this.setState({provSlider:e.target.value});
    }
  
    handleChangeMonSlider = (e)=>{
      this.setState({showError:false});
      this.setState({monSlider:e.target.value});
    }
  
    handleChangeDaySlider = (e)=>{
        this.setState({showError:false});
        this.setState({daySlider:e.target.value});
    }


render(){
    return(

        <Modal
          show={this.state.show}
          onHide={this.handleClose}
          keyboard={false}
          backdropClassName={styles.modalContainer}
        >
          <Modal.Header>
            <Modal.Title>Please verify your age</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          
          <div className={styles.slidecontainer}>

          <label className={styles.modalLabel}>Please select your Province:</label>

          <input
            type="range"
            id="modalProv"
            name="modalProv"
            className={styles.slider}
            min={0}
            max={12}
            step={1}
            value={this.state.provSlider}
            onChange={this.handleProvChangeSlider}
          />
          <div className={styles.sliderAge}>{this.state.provinces[this.state.provSlider]}</div>
         
          
            <label className={styles.modalLabel}>Please select your birth Year:</label>

              <input
                type="range"
                id="modalYear"
                name="modalYear"
                className={styles.slider}
                min={this.state.minYear}
                max={this.state.currentYear}
                step={1}
                value={this.state.yearSlider}
                onChange={this.handleChangeSlider}
              />

              <div className={styles.sliderAge}>{this.state.yearSlider}</div>
              
            
            {this.state.showMonth?
            <div>
              <label className={styles.modalLabel}>Please select your birth Month:</label>

              <input
                type="range"
                id="modalMonth"
                name="modalMonth"
                className={styles.slider}
                min={1}
                max={12}
                step={1}
                value={this.state.monSlider}
                onChange={this.handleChangeMonSlider}
              />

              <div className={styles.sliderAge}>{this.state.monSlider}</div>
              
            </div>
            :null

            }

            {this.state.showDay?
            <div>
              <label className={styles.modalLabel}>Please select your birth Date:</label>

              <input
                type="range"
                id="modalDay"
                name="modalDay"
                className={styles.slider}
                min={1}
                max={31}
                step={1}
                value={this.state.daySlider}
                onChange={this.handleChangeDaySlider}
              />

              <div className={styles.sliderAge}>{this.state.daySlider}</div>
              
            </div>
            :null

            }

          </div>
            
          {this.state.showError 
            ?
              <p className={styles.para} role="alert">
              You must be of legal age in your Region to enter the site.
              </p>
            :null
            }
          </Modal.Body>
          <Modal.Footer>
          
            <Button type="button" className={styles.btnSubmit} onClick={this.handleSubmit}>Submit</Button>
          </Modal.Footer>
        </Modal>
    )
}
}export default AgeGate;