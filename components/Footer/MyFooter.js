import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Footer from "components/Footer/Footer.js";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import contactUsStyle from "assets/jss/nextjs-material-kit-pro/pages/contactUsStyle.js";

const useStyles = makeStyles(contactUsStyle);



export default function MyFooter() {
  
    const classes = useStyles();
return(
<Footer
        theme="dark"
        content={
          <div>
            <ul className={classes.socialButtons}>

            <Button href="https://www.linkedin.com/in/ryan-maxwell-9a25325a" justIcon simple color="primary">
                        <i className="fab fa-linkedin" />
                      </Button>
                      <Button href="https://www.instagram.com/rymax10/" justIcon simple color="primary">
                        <i className="fab fa-instagram" />
                      </Button>
                      <Button href="https://www.facebook.com/ryan.maxwell.3382" justIcon simple color="primary">
                        <i className="fab fa-facebook-square" />
                      </Button>

            </ul>
            <div
              className={classNames(classes.pullCenter, classes.copyRight)}
            >
              Copyright &copy; {1900 + new Date().getYear()}{" "}
              <a href="/">Ryan Maxwell</a> All
                Rights Reserved.
              </div>
          </div>
        }
      >
        <div className={classes.footer}>
          <GridContainer>
            
            <GridItem >
              <h3>Ryan Maxwell</h3>
              <ul className={classes.linksVertical}>
                <li>
                  <a href="/programmer">Programmer Analyst</a>
                </li>
                <li>
                  <a href="/electrician">Licenced Electrician</a>
                </li>
                <li>
                  <a href="/contact">Contact Ryan</a>
                </li>
              </ul>
            </GridItem>

            
          </GridContainer>
        </div>
      </Footer>

 )
}